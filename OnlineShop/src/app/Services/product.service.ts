import { EventEmitter, Injectable } from '@angular/core';
import { product } from '../data';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OrderDetails } from '../model/order-details';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  cartData = new EventEmitter<product[] | []>();
  private baseUrl = 'http://localhost:8080/products/getProduct';

  constructor(private http: HttpClient) {}

  getAllProducts(): Observable<product[]> {
    return this.http.get<product[]>(this.baseUrl);
  }

  private baseUrl1 = 'http://localhost:8080/products/addProduct';

  addProduct(product: product): Observable<product> {
    return this.http.post<product>(`${this.baseUrl1}`, product);
  }

  private baseUrl2 = 'http://localhost:8080/products/delProduct/';

  deleteProduct(id: number) {
    return this.http.delete(`${this.baseUrl2}` + id);
  }

  private baseUrl3 = 'http://localhost:8080/products/getProducts/';

  getProductById(id: string): Observable<any> {
    const url = `${this.baseUrl3}` + id;
    return this.http.get<product>(url);
  }

  private baseUrl4 = 'http://localhost:8080/products/editProducts/';

  updateProduct(product: product) {
    return this.http.put<product>(
      `http://localhost:8080/products/editProducts/${product.id}`,
      product
    );
  }

  public getProductDetail(isSingleProductCheckout: any, id: any) {
    return this.http.get<product[]>(
      'http://localhost:8080/products/getProductDetail/' +
        isSingleProductCheckout +
        '/' +
        id
    );
  }
  public placeOrder(orderDetails: OrderDetails, isCartCheckout: any) {
    return this.http.post(
      'http://localhost:8080/Order/placeOrder/' + isCartCheckout,
      orderDetails
    );
  }

  public deleteCartItem(id: any) {
    return this.http.delete('http://localhost:8080/Cart/deleteCartItem/' + id);
  }

  public addToCart(id: any) {
    return this.http.get('http://localhost:8080/Cart/addToCart/' + id);
  }
  public getCartDetails() {
    return this.http.get('http://localhost:8080/Cart/getCartDetails');
  }

  PATH_OF_API = 'http://localhost:8083/student';

  public userLogin(user: any) {
    return this.http.post(this.PATH_OF_API + '/user', user, {
      responseType: 'text' as 'json',
    });
  }
}
