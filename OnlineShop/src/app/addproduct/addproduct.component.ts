import { Component } from '@angular/core';
import { ProductService } from '../Services/product.service';
import { product } from '../data';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.css'],
})
export class AddproductComponent {
  Product: product = {
    type: '',
    name: '',
    price: 0,
    image: '',
    id: 0,
    quantity: 0,
    productId: 0,
  };
  addProductMessage!: string;

  constructor(
    private productService: ProductService,
    private toastr: ToastrService
  ) {}

  addProduct() {
    this.productService.addProduct(this.Product).subscribe(
      (result) => {
        console.log(result);
        if (result) {
          this.addProductMessage = 'product is added successfully';
          this.toastr.success('Product added Successfully');
        }
      },
      (error) => {
        this.addProductMessage = 'product added failed';
        this.toastr.error('Product added to failed');
      }
    );
  }
}
