import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';

import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { CarsComponent } from './cars/cars.component';

import { MobilePhoneComponent } from './mobile-phone/mobile-phone.component';

import { CartComponent } from './cart/cart.component';
import { AddproductComponent } from './addproduct/addproduct.component';
import { ListComponent } from './list/list.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { UpdateComponent } from './update/update.component';
import { BuyProductComponent } from './buy-product/buy-product.component';
import { BuyProductResolverService } from './buy-product-resolver.service';
import { OrderConfirmationComponent } from './order-confirmation/order-confirmation.component';
import { HeaderComponent } from './header/header.component';
import { FormsModule } from '@angular/forms';
import { ProductDashboardComponent } from './product-dashboard/product-dashboard.component';

const routes: Routes = [
  { path: '', redirectTo: 'Login', pathMatch: 'full' },
  { path: 'Home', component: HomeComponent },
  { path: 'dashboard', component: ProductDashboardComponent },

  { path: 'Login', component: LoginComponent },
  { path: 'SignUp', component: SignUpComponent },
  { path: 'header', component: HeaderComponent },
  { path: 'Cars', component: CarsComponent },

  { path: 'MobilePhone', component: MobilePhoneComponent },

  { path: 'Cart', component: CartComponent },
  {
    path: 'AddProduct',
    component: AddproductComponent,
  },
  { path: 'List', component: ListComponent },
  { path: 'Detail/:id', component: ProductDetailsComponent },
  { path: 'checkout', component: CheckoutComponent },
  { path: 'Update/:id', component: UpdateComponent },
  {
    path: 'buyProduct',
    component: BuyProductComponent,
    resolve: { productDetails: BuyProductResolverService },
  },
  {
    path: 'orderConfirm',
    component: OrderConfirmationComponent,
    data: { roles: ['User'] },
  },
  {
    path: 'menu',
    loadChildren: () =>
      import('./home/home-module/home-module.module').then(
        (m) => m.HomeModuleModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), FormsModule],
  exports: [RouterModule],
})
export class AppRoutingModule {}
