import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { product } from './data';
import { Observable, pipe } from 'rxjs';
import { ProductService } from './Services/product.service';

@Injectable({
  providedIn: 'root',
})
export class BuyProductResolverService implements Resolve<product[]> {
  constructor(private productService: ProductService) {}
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): product[] | Observable<product[]> | Promise<product[]> {
    const Id = route.paramMap.get('Id');
    const isSingleProductCheckout = route.paramMap.get(
      'isSingleProductCheckout'
    );
    return this.productService.getProductDetail(isSingleProductCheckout, Id);
  }
}
