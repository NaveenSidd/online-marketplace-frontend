import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { OrderDetails } from '../model/order-details';
import { ActivatedRoute, Router } from '@angular/router';
import { product } from '../data';
import { ProductService } from '../Services/product.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-buy-product',
  templateUrl: './buy-product.component.html',
  styleUrls: ['./buy-product.component.css'],
})
export class BuyProductComponent implements OnInit {
  productDetails: product[] = [];
  isSingleProductCheckout: any;
  orderDetails: OrderDetails = {
    fullName: '',
    fullAddress: '',
    contactNumber: '',
    alternateContactNumber: '',
    orderProductQuantityList: [],
  };
  constructor(
    private activatedRoute: ActivatedRoute,
    private productServive: ProductService,
    private router: Router,
    private toastr: ToastrService
  ) {}
  ngOnInit(): void {
    this.productDetails = this.activatedRoute.snapshot.data['productDetails'];
    this.isSingleProductCheckout = this.activatedRoute.snapshot.paramMap.get(
      'isSingleProductCheckout'
    );
    this.productDetails.forEach((x) =>
      this.orderDetails.orderProductQuantityList.push({ id: x.id, quantity: 1 })
    );
    console.log(this.productDetails);
    console.log(this.orderDetails);
  }
  placeOrder(orderForm: NgForm) {
    this.productServive
      .placeOrder(this.orderDetails, this.isSingleProductCheckout)
      .subscribe(
        (resp) => {
          console.log(resp);
          this.toastr.success('you Order is confirmed');
          this.router.navigate(['/orderConfirm']);
          orderForm.reset();
        },
        (err) => {
          console.log(err);
          this.toastr.error('Some Error Occured');
        }
      );
  }
  getQuantityForProduct(id: any) {
    const filteredProduct = this.orderDetails.orderProductQuantityList.filter(
      (productQuantity) => productQuantity.id === id
    );
    return filteredProduct[0].quantity;
  }

  getCalculatedTotal(id: any, price: any) {
    const filteredProduct = this.orderDetails.orderProductQuantityList.filter(
      (productQuantity) => productQuantity.id === id
    );
    return filteredProduct[0].quantity * price;
  }

  onQuantityChanged(q: any, id: any) {
    this.orderDetails.orderProductQuantityList.filter(
      (orderProduct) => orderProduct.id === id
    )[0].quantity = q;
  }

  getCalculatedGrantTotal() {
    let grantTotal = 0;
    this.orderDetails.orderProductQuantityList.forEach((productQuantity) => {
      const price = this.productDetails.filter(
        (product) => product.id === productQuantity.id
      )[0].price;
      grantTotal = grantTotal + price * productQuantity.quantity;
    });
    return grantTotal;
  }
}
