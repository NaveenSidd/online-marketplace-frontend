import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { product } from './data';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  private apiUrl = 'http://localhost:8080/api/cart';

  constructor(private http: HttpClient) {}

  addToCart(product: product): Observable<string> {
    return this.http.post<string>(`${this.apiUrl}/add`, product);
  }

  getCart(): Observable<product[]> {
    return this.http.get<product[]>(this.apiUrl);
  }
}
