import { Component, OnInit } from '@angular/core';
import { ProductService } from '../Services/product.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// import { Cart } from '../data';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
})
export class CartComponent implements OnInit {
  cartDetails: any[] = [];

  constructor(
    private productService: ProductService,
    private router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.productService.getCartDetails().subscribe(
      (response: any) => {
        console.log(response);
        this.cartDetails = response;
      },
      (error) => {
        console.log(error);
      }
    );
  }
  delete(id: any) {
    console.log(id);
    this.productService.deleteCartItem(id).subscribe(
      (res) => {
        console.log(res);
        this.toastr.success('your cart product is deleted');
      },
      (err) => {
        console.log(err);
      }
    );
  }

  checkout() {
    this.router.navigate([
      '/buyProduct',
      {
        isSingleProductCheckout: false,
        Id: 0,
      },
    ]);
  }
}
