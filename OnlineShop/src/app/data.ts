export interface product {
  type: string;
  name: string;
  price: number;
  image: string;
  id: number;
  quantity: number;
  productId: number;
}
