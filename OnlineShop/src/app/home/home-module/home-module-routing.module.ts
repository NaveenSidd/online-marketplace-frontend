import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../home.component';
import { AddproductComponent } from 'src/app/addproduct/addproduct.component';
import { ListComponent } from 'src/app/list/list.component';
import { CartComponent } from 'src/app/cart/cart.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      { path: 'AddProduct', component: AddproductComponent },
      { path: 'List', component: ListComponent },
      { path: 'Cart', component: CartComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeModuleRoutingModule {}
