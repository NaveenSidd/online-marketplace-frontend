import { Component, OnInit } from '@angular/core';
import { product } from '../data';
import { ProductService } from '../Services/product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  totalLength: any;
  page: number = 1;
  searchText: any;
  products: product[] = [];
  productQuantity: number = 1;
  constructor(private productService: ProductService, private router: Router) {}
  ngOnInit() {
    this.productService.getAllProducts().subscribe((result) => {
      this.products = result;
      console.log(this.products);
    });
  }

  onSearchTextEntered(searchValue: any) {
    this.searchText = searchValue;
    console.log(searchValue);
  }
  logout() {
    const isConfirmed = window.confirm('Are you sure you want to log out?');

    // If the user confirms, proceed with logout and navigate to login page
    if (isConfirmed) {
      this.router.navigate(['Login']);
    }
    // this.router.navigate(['Login']).then(() => {
    //   if (confirm('Are you sure!')) window.location.reload();
    // });
  }
}
