import { Component, OnInit } from '@angular/core';
import { product } from '../data';
import { ProductService } from '../Services/product.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent implements OnInit {
  products: product[] = [];

  constructor(
    private productService: ProductService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.productService.getAllProducts().subscribe((data) => {
      this.products = data;
      console.log(this.products);
    });
  }

  deleteProduct(id: number) {
    this.productService.deleteProduct(id).subscribe(
      (result) => {
        alert('Your Product cannot deleted');
      },
      (error) => {
        // alert('Your Product Deleted successfully');
        this.toastr.success('your product is deleted ');
      }
    );
  }
  // editProduct(productId: number, updatedProductData: product): void {
  //   this.productService.updateProduct(productId, updatedProductData).subscribe(
  //     (response) => {
  //       // Handle success
  //       console.log('Product updated:', response);
  //     },
  //     (error) => {
  //       // Handle error
  //       console.error('Error updating product:', error);
  //     }
  //   );
  // }
}
