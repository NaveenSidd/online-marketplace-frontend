import { Component } from '@angular/core';
import { ProductService } from '../Services/product.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormValidationServiceService } from '../serviceFolder/form-validation-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  userdetails = new loginVo();

  email: string = '';
  password: string = '';

  constructor(
    private proService: ProductService,
    private router: Router,
    private toastr: ToastrService,
    public formValidService: FormValidationServiceService
  ) {}

  login() {
    this.userdetails.email = this.email;
    this.userdetails.password = this.password;
    console.log(this.userdetails.email);

    this.proService.userLogin(this.userdetails).subscribe(
      (response) => {
        // alert('login successfull');

        console.log(response);
        this.toastr.success('Login Success');
        this.router.navigateByUrl('Home');
      },
      (error) => {
        // alert('login failed');
        this.toastr.error('Login Failed');
      }
    );
  }
}

export class loginVo {
  email!: string;
  password!: string;
}
