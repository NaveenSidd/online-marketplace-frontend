// import { HttpClient } from '@angular/common/http';
// import { Injectable } from '@angular/core';
// import { User } from './user';
// import { Observable } from 'rxjs';

// @Injectable({
//   providedIn: 'root',
// })
// export class LoginuserService {
//   private baseUrl = 'http://localhost:8085/student/user';
//   constructor(private httpClient: HttpClient) {}

//   loginUser(user: User): Observable<object> {
//     console.log(user);
//     return this.httpClient.post(`${this.baseUrl}`, user);
//   }
// }

// src/app/login.service.ts
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  PATH_OF_API = 'http://localhost:8083/student';
  loginUser: any;

  constructor(private http: HttpClient) {}

  login(user: any) {
    return this.http.post(this.PATH_OF_API + '/user', user);
  }
}
