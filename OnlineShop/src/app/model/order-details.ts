import { OrderQuantity } from './order-quantity';

export interface OrderDetails {
  fullName: string;
  fullAddress: string;
  contactNumber: string;
  alternateContactNumber: string;
  orderProductQuantityList: OrderQuantity[];
}
