export class StudentRegister {
  id?: number;
  firstname?: string;
  lastname?: string;
  email?: string;
  password?: string;
}
