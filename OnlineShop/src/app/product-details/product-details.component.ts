import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../Services/product.service';
import { product } from '../data';
import { ToastrService } from 'ngx-toastr';
// import { Cart } from '../data';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css'],
})
export class ProductDetailsComponent implements OnInit {
  productData: undefined | product;
  productQuantity: number = 1;
  productId: any;
  constructor(
    private activeRoute: ActivatedRoute,
    private productService: ProductService,
    private router: Router,
    private toastr: ToastrService
  ) {}
  ngOnInit(): void {
    let id = this.activeRoute.snapshot.paramMap.get('id');
    console.log(id);
    id &&
      this.productService.getProductById(id).subscribe((result) => {
        this.productData = result;
      });
  }
  // handleQuantity(val: string) {
  //   if (this.productQuantity < 20 && val === 'max') {
  //     this.productQuantity += 1;
  //   }
  //   if (this.productQuantity > 1 && val === 'min') {
  //     this.productQuantity -= 1;
  //   }
  // }
  addToCart(id: any) {
    this.productService.addToCart(id).subscribe(
      (response) => {
        console.log(response);
        this.toastr.success('adding to your cart');
      },
      (error) => {
        console.log(error);
      }
    );
  }
  buyProduct(id: any) {
    this.router.navigate([
      '/buyProduct',
      {
        isSingleProductCheckout: true,
        Id: id,
      },
    ]);
  }
}
