import { TestBed } from '@angular/core/testing';

import { NetworkCallService1Service } from './network-call-service1.service';

describe('NetworkCallService1Service', () => {
  let service: NetworkCallService1Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NetworkCallService1Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
