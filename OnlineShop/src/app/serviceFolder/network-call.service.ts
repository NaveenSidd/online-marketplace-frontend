import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StudentRegister } from '../model/student-register';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class NetworkCallService {
  private URLRegisteration = 'http://localhost:8083/student/Registeration';
  constructor(public httpClient: HttpClient) {}

  createStudentRegisteration(studentRegi: StudentRegister): Observable<object> {
    return this.httpClient.post(this.URLRegisteration, studentRegi);
  }
}
