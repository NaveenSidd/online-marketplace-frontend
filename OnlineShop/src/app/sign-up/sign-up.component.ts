import { Component } from '@angular/core';
// import { FormGroup, FormBuilder } from '@angular/forms';
import { FormValidationServiceService } from '../serviceFolder/form-validation-service.service';
import { NetworkCallService } from '../serviceFolder/network-call.service';
import { StudentRegister } from '../model/student-register';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css'],
})
export class SignUpComponent {
  userRegisterationModel: StudentRegister = new StudentRegister();
  constructor(
    public formValidService: FormValidationServiceService,
    public UrlService: NetworkCallService,
    private toastr: ToastrService // public dialogReferrence: MatDialogRef<SignUpComponent>
  ) {}

  SubmitData() {
    this.UrlService.createStudentRegisteration(
      this.userRegisterationModel
    ).subscribe(
      (data) => {
        // alert('Successfully Registerd');
        this.toastr.success('Registeration Successful');
      },
      (error) => {
        // alert('Registeration Error')
        this.toastr.error('Registeration error');
      }
    );

    // this.onClose();
  }
  // onClose() {
  //   this.dialogReferrence.close();
  // }
}
