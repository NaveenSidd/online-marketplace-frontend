import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../Services/product.service';
import { product } from '../data';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css'],
})
export class UpdateComponent implements OnInit {
  productData!: product;
  productMessage: undefined | string;
  constructor(
    private route: ActivatedRoute,
    private product: ProductService,
    private toastr: ToastrService
  ) {}
  ngOnInit(): void {
    let productId = this.route.snapshot.paramMap.get('id');
    console.warn(productId);
    productId &&
      this.product.getProductById(productId).subscribe((data) => {
        console.warn(data);
        this.productData = data;
      });
  }
  submit(data: any) {
    console.log(data);

    if (this.productData) {
      data.id = this.productData.id;
    }

    this.product.updateProduct(this.productData).subscribe((result) => {
      if (result) {
        this.productMessage = 'product is Updated';
        this.toastr.success('your product is update');
      }
    });
    console.warn(data);
    setTimeout(() => {
      this.productMessage = undefined;
    }, 3000);
  }
}
